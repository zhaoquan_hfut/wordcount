package crawler;

import nlp.Nlp;
import mysql.Mysql;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;

public class MyCrawler extends WebCrawler {
    /**
     * 正则表达式匹配指定的后缀文件
     */
    private static final Pattern FILTERS = Pattern.compile(
            ".*(\\.(css|js|bmp|gif|jpe?g|png|tiff?|mid|mp2|mp3|mp4|wav|avi" +
                    "|mov|mpeg|ram|m4v|pdf|rm|smil|wmv|swf|wma|zip|rar|gz))$");


    /**
     * 这个方法主要是决定哪些url我们需要抓取，返回true表示是我们需要的，返回false表示不是我们需要的Url
     * 第一个参数referringPage封装了当前爬取的页面信息 第二个参数url封装了当前爬取的页面url信息
     * 在这个例子中，我们指定爬虫忽略具有css，js，git，...扩展名的url，只接受以“http://www.ics.uci.edu/”开头的url。
     * 在这种情况下，我们不需要referringPage参数来做出决定。
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();// 得到小写的url
        return !FILTERS.matcher(href).matches() // 正则匹配，过滤掉我们不需要的后缀文件
                && href.startsWith("https://ruby-china.org/topics");// 只接受以“http://www.ics.uci.edu/”开头的url
    }

    /**
     * 当一个页面被提取并准备好被你的程序处理时，这个函数被调用。
     */
    @Override
    public void visit(Page page) {
        //String url = page.getWebURL().getURL();// 获取url
        //System.out.println("URL: " + url);

        HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
        String html = htmlParseData.getHtml();//页面html内容
        Document doc = Jsoup.parse(html);//采用jsoup解析html

        //使用选择器的时候需要了解网页中的html规则

        Elements elements = doc.select("div.topic-detail.card");

        String title = elements.select("div.media-body > h1 > span.title").text();//标题

        Elements elements1 = elements.select("div.card-body.markdown.markdown-toc");//内容
        String content = "";
        for (Element element : elements1) {
            content = content + element.select("p").text();
        }

        Nlp nlp = new Nlp();
        List wordList = nlp.hanLP(content);
        nlp.wordFrequency(wordList);
        String[] words = nlp.getWords();
        int[] number = nlp.getNumber();

        Mysql mysql = new Mysql();
        try {
            mysql.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (StringUtils.isNotBlank(title)) {
                int news_id = mysql.insertIntoNews(title,content);
                for (int i = 0; i < number.length; i++) {
                    mysql.insertIntoWordFrequency(news_id, words[i], number[i]);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

