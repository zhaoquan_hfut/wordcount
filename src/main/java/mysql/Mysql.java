package mysql;

import java.sql.*;

public class Mysql {
    public Statement stat;
    public Connection conn;

    public void connect() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/db_news?useUnicode=true&characterEncoding=utf-8";
        conn = DriverManager.getConnection(url, "root", "");
        stat = conn.createStatement();
    }

    public void close() throws SQLException {
        stat.close();
        conn.close();
    }

    public int insertIntoNews(String news_name, String content) throws SQLException {
        stat.executeUpdate("insert into tb_news (news_name,news_content) values('"+news_name+"','"+content+"');");
        ResultSet result = stat.executeQuery("select news_id from tb_news where news_name='"+news_name+"';");
        result.next();
        return result.getInt("news_id");
    }

    public void insertIntoWordFrequency(int news_id, String word, int number) throws SQLException {
        stat.executeUpdate("insert into tb_word_frequency (news_id,word,frequency) values("+news_id+",'"+word+"',"+number+");");
    }

    public void selectWordFrequency(int news_id) throws SQLException {
        ResultSet result = stat.executeQuery("select word,frequency from tb_word_frequency where news_id="+news_id+";");
        while (result.next())
        {
            System.out.println( result.getString("word")+ " " + result.getInt("frequency"));
        }
    }

    public void selectNewsId(String word) throws SQLException {
        ResultSet result = stat.executeQuery("select news_id from tb_word_frequency where word='"+word+"';");
        while (result.next())
        {
            System.out.println( result.getInt("news_id"));
        }
    }

    public void selectNewsNumber(String word) throws SQLException {
        ResultSet result = stat.executeQuery("select count(*) from tb_word_frequency where word='"+word+"';");
        result.next();
        System.out.println(result.getInt(1));
    }

    public static void main(String[] args) throws Exception {
        Mysql mysql = new Mysql();
        mysql.connect();
        mysql.selectWordFrequency(4);
        mysql.selectNewsId("富强");
        mysql.selectNewsNumber("富强");
        mysql.close();
    }
}


//        创建数据库wordcount
//        stat.executeUpdate("create database wordcount");
//
//        打开创建的数据库
//        stat.close();
//        conn.close();
//        url = "jdbc:mysql://localhost:3306/wordcount?useUnicode=true&characterEncoding=utf-8";
//        conn = DriverManager.getConnection(url, "root", "");
//        stat = conn.createStatement();
//
//        创建表test
//        stat.executeUpdate("create table news1(word varchar(80) primary key,num int)");
//
//        添加数据
//
//        查询数据
//        ResultSet result = stat.executeQuery("select * from news1");
//        while (result.next())
//        {
//            System.out.println( result.getString("word")+ " " + result.getInt("num"));
//        }
//
//        关闭数据库
//        result.close();
//    public static void main(String[] args) throws Exception {
//        Mysql mysql = new Mysql();
//        mysql.connect();
//
//    }


