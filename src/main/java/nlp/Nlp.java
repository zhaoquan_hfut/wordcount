package nlp;

import mysql.Mysql;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.tokenizer.StandardTokenizer;

/**
 * 分词、词频统计并生成直方图
 *
 * @author zq
 * @date 2018-11-15
 */
public class Nlp {

    static Logger logger = Logger.getLogger(Nlp.class.getName());
    private String[] words;
    private int[] number;

    public Nlp() {
    }

    public String[] getWords() {
        return words;
    }

    public int[] getNumber() {
        return number;
    }

    /**
     * 读取txt文件生成字符串
     */
    public String loadFile(String filename) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String source = "";
        String s = "";
        while ((s = reader.readLine()) != null) {
            source = source + s;
        }
        return source;
    }

    /**
     * 分词
     */
    public List hanLP(String source) {
        source = source.replaceAll("[^\u4E00-\u9FA5]", "");//////只取中文
        HanLP.Config.ShowTermNature = false; ////////关闭标注词性
        List wordList = StandardTokenizer.segment(source);
        return wordList;
    }

    /**
     * 词频统计
     */
    public void wordFrequency(List wordList) {
        Set<String> wordSet = new HashSet<String>();
        Collection numberCollection;
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < wordList.size(); i++) {
            if (wordSet.add(wordList.get(i).toString())) {
                map.put(wordList.get(i).toString(), 1);
            } else {
                map.put(wordList.get(i).toString(), map.get(wordList.get(i).toString()) + 1);
            }
        }
        numberCollection = map.values();
        this.words = toStringArray(wordSet);
        this.number = toIntArray(numberCollection);
    }

    /**
     * 导入数据库
     */
    public void importToMysql(int news_id, String[] words, int[] number) throws Exception {
        Mysql mysql = new Mysql();
        mysql.connect();
        for(int i = 0; i < words.length; i++){
            mysql.insertIntoWordFrequency(news_id,words[i],number[i]);
        }
        mysql.close();
    }

    /**
     * Collection->int[]
     */
    public int[] toIntArray(Collection collection) {
        Integer[] integerArray = new Integer[collection.size()];
        collection.toArray(integerArray);
        int[] intArray = new int[integerArray.length];
        for (int i = 0; i < integerArray.length; i++) {
            intArray[i] = integerArray[i].intValue();
        }
        return intArray;
    }

    /**
     * Set->String[]
     */
    public String[] toStringArray(Set set) {
        String[] newString = new String[set.size()];
        set.toArray(newString);
        return newString;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here

        //读取文件
        Nlp nlp = new Nlp();
        String file = "word.txt";
        String source = "";
        try {
            source = nlp.loadFile(file);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        //分词并词频统计
        List wordList = nlp.hanLP(source);
        System.out.println(wordList);
        nlp.wordFrequency(wordList);

        //生成直方图
        Histogram histogram = new Histogram();
        BufferedImage image = histogram.paintPlaneHistogram("词频直方图",
                nlp.number, nlp.words, new Color[]{Color.BLACK});
        File output = new File("C:\\Users\\35124\\histogram.jpg");
        try {
            ImageIO.write(image, "jpg", output);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //导入数据库
        nlp.importToMysql(4,nlp.words,nlp.number);

    }

}

